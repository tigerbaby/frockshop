<?php

/**
 * Implementation of hook_views_data().
 */
function uc_rentals_views_data() {
  $data = array();

  $data['uc_rentals_items']['table']['grou'] = t('UC Rentals');

  $data['uc_rentals_items']['table']['join'] =  array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['uc_rentals_items_out']['table']['group'] = t('UC Rentals');

  $data['uc_rentals_items_out']['table']['join'] = array(
    'node' => array(
      'left_table' => 'uc_rentals_items',
      'left_field' => 'rental_id',
      'field' => 'rental_id',
    ),
  );

  $data['uc_rentals_items_out']['out_date'] = array(
    'title' => t('Out Date'), 
    'help' => t('Out Date column'),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['uc_rentals_items_out']['expected_back'] = array(
    'title' => t('Expected Back'), 
    'help' => t('Expected Back column'),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}
