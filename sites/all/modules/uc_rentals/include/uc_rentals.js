	$(document).ready(function(){
        // Drupal.settings.uc_rental_Blackout = new Array('7/30/2011');
        $('#edit-date-popup-datepicker-popup-0-wrapper input').click(function(){ blackout_days(); });
        $('#ui-datepicker-div').change(function(){ blackout_days(); });
        setInterval('blackout_days()', 200);  
	setRentalDateField();
    });
	
function setRentalDateField(){
	var wrapper = $('#edit-attributes-16-date-popup-datepicker-popup-0-wrapper');
	$(wrapper).children('input').attr('readonly','readonly');
	$(wrapper).children('input').val('SELECT DATE');
	$(wrapper).children('input').css('padding','2px 5px');
	$(wrapper).children('div.description').css('display','none');
	$('input.node-add-to-cart').val('RESERVE NOW');
	$('input.node-add-to-cart').css('background-color','#ff339a');	
	$('input.node-add-to-cart').css('width','114px');	
}

function blackout_days(){		
	function isNotGoodDate(item){
		var isNot = false;
		if($(item).hasClass('ui-datepicker-unselectable'))isNot = true;
		if($(item).hasClass('blocked-date'))isNot = true;
		return isNot;
	}
	function setNextDate(item, color, numCells){
		function getNextItem(lastItem){
			var tmp = $(lastItem).next();
			if(!$(tmp).length){
				var nextRow = $(item).parent().next();
				return $('td:first-child',nextRow);
			}
			else return tmp;
		}
		nextItem = getNextItem(item);
		for(var i = 0; i < numCells; i++){
			if(isNotGoodDate(nextItem))return;
			$(nextItem).css('background-color',color);
			nextItem = getNextItem(nextItem);
		}
	}
	function highlightRentalPeriod(item,turnOn){
		if(isNotGoodDate(item))return;
		var numCells = $('#edit-attributes-15-18').is(':checked') ? 6 : 3;
		if(turnOn){
			$(item).css('background-color','#ff339a');
			setNextDate(item,'#ff99cb',numCells);
		}
		else {
			$(item).css('background-color','#ffffff');			
			setNextDate(item,'#ffffff',numCells);
		}
	}

		function blockIt(item,isCell){
//	                $(item).css('opacity', '.6');
			$(item).css('background-color','#999999');
			if(isCell){
				$(item).attr('onclick', '');
				$(item).addClass('blocked-date');
			}
			else {
	                	$(item).parent().attr('onclick', '');
				$(item).parent().addClass('blocked-date');
			}
		}		
            Drupal.settings.uc_rentals.SelectedMonth = $('.ui-datepicker-new-month').val();
            Drupal.settings.uc_rentals.SelectedYear = $('.ui-datepicker-new-year').val();
            var thisDate = new Date(),lastYear,earlierInYear,thisYear,thisMonth,fiveMonth,nextYear;
            Drupal.settings.uc_rentals.thisMonth = thisDate.getMonth();
            Drupal.settings.uc_rentals.thisYear = thisDate.getFullYear();
            Drupal.settings.uc_rentals.thisDay = thisDate.getDate() + 2;
            lastYear = Drupal.settings.uc_rentals.SelectedYear < Drupal.settings.uc_rentals.thisYear;
            thisYear = Drupal.settings.uc_rentals.SelectedYear == Drupal.settings.uc_rentals.thisYear;
            nextYear = Drupal.settings.uc_rentals.thisYear + 1;
            earlierInYear = thisYear && Drupal.settings.uc_rentals.SelectedMonth < Drupal.settings.uc_rentals.thisMonth;
            thisMonth = Drupal.settings.uc_rentals.SelectedMonth == Drupal.settings.uc_rentals.thisMonth;
            //console.log(Drupal.settings.uc_rentals.SelectedMonth,Drupal.settings.uc_rentals.SelectedYear);
            
            function getFiveMonth(selM){
            	if(lastYear)return false;
            	if(earlierInYear)return false;
            	if(thisMonth && thisYear)return false;
            	var tM = Drupal.settings.uc_rentals.thisMonth + 1, fM = tM + 5;
            	fM = fM > 12 ? fM - 12 : fM;
            	if(fM > tM){
	            	if(thisYear && (selM < fM))return true;
            	}
            	else if(selM < fM && Drupal.settings.uc_rentals.SelectedYear == nextYear )return true;
            	else return false;
            }
            
            fiveMonth = getFiveMonth(Drupal.settings.uc_rentals.SelectedMonth);           
            
              $('.ui-datepicker-days-cell').each(function(){ 
              	if(lastYear)blockIt(this,true);
		else if(earlierInYear)blockIt(this,true);
		else if(thisMonth && thisYear){//console.log(thisMonth);
			if($('a',this).html() < Drupal.settings.uc_rentals.thisDay + 1)blockIt(this,true);
		}
		else if(!fiveMonth)blockIt(this,true);
              });

            
            // Loop through blackout dates
            for( x in Drupal.settings.uc_rentals.blackout_days ){
                var blackout_day = new Date(Drupal.settings.uc_rentals.blackout_days[x]);
                if(blackout_day.getMonth() == Drupal.settings.uc_rentals.SelectedMonth && blackout_day.getFullYear() == Drupal.settings.uc_rentals.SelectedYear){
                  $('.ui-datepicker-days-cell a').each(function(){
                    if($(this).html() == blackout_day.getDate()){
			    blockIt(this,false);
                    }
                  });
                }
            }
	$('td.ui-datepicker-days-cell').each(function(){		
		if($(this).hasClass('hover-activated'))return;
		$(this).addClass('hover-activated');
		$(this).hover(
			function () {highlightRentalPeriod(this,true);},
			function () {highlightRentalPeriod(this,false);}
		);
	});
}
