<?php

/**
 * @file
 *
 * Theme functions
 */

/**
 * Implements HOOK_theme().
 *
function om_theme(&$existing, $type, $theme, $path) {
  //drupal_set_message("<pre>" . print_r($existing,true) . "</pre>");

  if ($type == 'theme') {
    dsm($existing, true);
  }
  return array();
}
*/

/**
 * Implements HOOK_theme().
 */
function om_theme() {
  return array(
    'region' => array(
      'arguments' => array('region_name' => NULL, 'region' => NULL, 'region_classes' => NULL, 'region_inner' => 0),
    ),
    'identity' => array(
      'arguments' => array('logo' => NULL, 'site_name' => NULL, 'site_slogan' => NULL, 'front_page' => NULL),
    ),
    'content_elements' => array(
      'arguments' => array('mission' => NULL, 'tabs' => NULL, 'title' => NULL),
    ),    
    'search_box' => array(
      'arguments' => array('search_box' => NULL),
    ),  
    'menu' => array(
      'arguments' => array('menu_name' => NULL, 'menu' => NULL, 'menu_tree' => NULL),
    ),              
  );
}

/** 
 * Logo, Site Name, Site Slogan
 */
function om_identity($logo, $site_name, $site_slogan, $front_page) {
  if (!empty($logo) || !empty($site_name) || !empty($site_slogan)) { 
    $out = '<div id="logo-title">';
    if (!empty($logo)) { 
      $out .= '<a href="' . $front_page . '" title="' . t('Home') . '" rel="home" id="logo">';
      $out .= '<img src="' . $logo . '" alt="' . t('Home') . '" />';
      $out .= '</a>';
    }
    if (!empty($site_name) || !empty($site_slogan)) { 
      $out .= '<div id="name-and-slogan">';
      if (!empty($site_name)) {
        $out .= '<h1 id="site-name">';
        $out .= '<a href="' . $front_page . '" title="' . t('Home') . '" rel="home">' . $site_name . '</a>';
        $out .= '</h1>';
      }
      if (!empty($site_slogan)) {
        $out .= '<div id="site-slogan">' . $site_slogan . '</div>';
      }
      $out .= '</div> <!-- /#name-and-slogan -->';
   }    
   $out .= '</div> <!-- /#logo-title -->';
  return $out;
  }
}

/**
 * Search Box
 */
function om_search_box($search_box) {
  if (!empty($search_box)) {
    return '<div id="search-box">' . $search_box . '</div>';
  }
}

/**
 * Primary, Secondary Menus
 */
function om_menu($menu_name = NULL, $menu = NULL, $menu_tree = NULL) {
  if (!empty($menu)) { 
    return '<div id="menubar-' . $menu_name . '" class="menubar">' . $menu_tree . '</div>';
  }
}
/**
 * Overriding the menu item behavior
 *
 */
function om_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' ' . $extra_class;
  }
  
  if ($in_active_trail) {
    $class .= ' active-trail';
  }
  return '<li class="' . $class . ' ' . $link . $menu . "</li>\n";  
}

/**
 * Adding menu id to li tag classes and not to a tag
 *
 */
function om_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }
  //dsm($link);
  if(isset($link['tab_root'])) {
    //This works for local tabs
    return l($link['title'], $link['href'], $link['localized_options']);
  }
  else {
    //This is for normal menus
    return 'menu-' . $link['mlid'] . '">' . l($link['title'], $link['href'], $link['localized_options']);
  }
}


/**
 * Process all region formats
 */
function om_region($region_name = NULL, $region = NULL, $region_classes = NULL, $region_inner = 0) {
  if ($region) { 
    if ($region_inner == 1) { 
      $div_prefix = '<div id="' . $region_name . '" class="region ' . $region_classes . '"><div id="' . $region_name . '-inner" class="region-inner">'; 
      $div_suffix = '<div class="clearfix"></div></div><!-- /#' . $region_name . '-inner --></div><!-- /#' . $region_name . ' -->'; 
    }
    else {
      $div_prefix = '<div id="' . $region_name . '" class="region ' . $region_classes . '">'; 
      $div_suffix = '<div class="clearfix"></div></div><!-- /#' . $region_name . ' -->'; 
    }
    //dsm($region);
    return $div_prefix . $region . $div_suffix;
  }
}

/**
 * Breadcrumbs
 */
function om_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div id="breadcrumb">' . implode(' » ', $breadcrumb) . '</div>'; 
  }
}


/**
 * Process all content elements
 */
function om_content_elements($mission = NULL, $tabs = NULL, $title = NULL) {
  $out = '';
  if (!empty($mission)) { 
    $out .= '<div id="mission">' . $mission .'</div>'; 
  }
  if (!empty($tabs)) { 
    $out .= '<div class="tabs">' . $tabs .'</div>'; 
  }
  if (!empty($title)) { 
    $out .= '<h1 class="title" id="page-title">' . preg_replace('/\[break\]/', '<br />', $title) . '</h1>'; 
  }
  return $out;
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 * - Ensure an ID starts with an alpha character by optionally adding an 'id'.
 * - Replaces any character except alphanumeric characters with dashes.
 * - Converts entire string to lowercase.
 *
 * @param $string
 *   The string
 * @return
 *   The converted string
 */
function om_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}

/**
 * Override or insert om variables into the templates.
 */
function om_preprocess_page(&$vars) {
  $vars['head_title'] = preg_replace('/\[break\]/', '', $vars['head_title']);
  $vars['tabs2'] = menu_secondary_local_tasks();
  // Generate menu tree from source of primary links
  $vars['main_menu'] = $vars['primary_links'];
  $vars['secondary_menu'] = $vars['secondary_links'];
  //$vars['main_menu_tree'] = menu_tree(variable_get('menu_primary_links_source', 'primary-links'));
  //Override main_menu_tree variable use:
  $vars['main_menu_array']  = menu_tree_page_data('primary-links');
  //dsm($vars['main_menu_array']);
  $vars['main_menu_tree']  = menu_tree_output($vars['main_menu_array']);

  $vars['secondary_menu_tree'] = menu_tree(variable_get('menu_secondary_links_source', 'secondary-links'));


  // Hook into color.module
  if (module_exists('color')) { 
    _color_page_alter($vars); 
  }
  $vars['closure'] .= '<div id="legal"><a href="http://www.drupal.org/project/om">OM Base Theme</a> ' . date('Y') . ' | V6.x-2.x | by <a href="http://www.danielhonrade.com">Daniel Honrade</a></div>';
  // recognize url aliases as file page templates, ex. page-title.tpl.php
  if (module_exists('path')) {
    $alias = drupal_get_path_alias(str_replace('/edit', '', $_GET['q']));
    if ($alias != $_GET['q']) {
      $template_filename = 'page';
      foreach (explode('/', $alias) as $path_part) {
        $template_filename = $template_filename . '-' . $path_part;
        $vars['template_files'][] = $template_filename;
      }
    }
  }
  // aggregate css files
  $preprocess_css = variable_get('preprocess_css', 0);
  $query_string = '?' . substr(variable_get('css_js_query_string', '0'), 0, 1);
  if (!$preprocess_css) {
    $styles = '';
    foreach ($vars['css'] as $media => $types) {
      $import = '';
      $counter = 0;
      foreach ($types as $files) {
        foreach ($files as $css => $preprocess) {
          $import .= '@import "'. base_path() . $css . $query_string .'";'."\n";
          $counter++;
          if ($counter == 15) {
            $styles .= "\n".'<style type="text/css" media="'. $media .'">'."\n". $import .'</style>';
            $import = '';
            $counter = 0;
          }
        }
      }
      if ($import) {
        $styles .= "\n".'<style type="text/css" media="'. $media .'">'."\n". $import .'</style>';
      }
    }
    if ($styles) {
      $vars['styles'] = $styles;
    }
  }
  
  // kill the old left & right sidebars effect on body classes
  $old_sidebars = array('no-sidebars', 'two-sidebars', 'one-sidebar', 'sidebar-left', 'sidebar-right');
  foreach ($old_sidebars as $old_sidebars_key => $old_sidebars_class) {
    $vars['body_classes'] = str_replace($old_sidebars_class, '', $vars['body_classes']);
  }

  // substitute $sidebar_left and $sidebar_right for $left and $right
  // Set up layout variable.
  $vars['layout'] = 'none';  
      
  if (!empty($vars['sidebar_first'])) {
    $vars['layout'] = 'first';
  }
  if (!empty($vars['sidebar_second'])) {
    $vars['layout'] = ($vars['layout'] == 'first') ? 'both' : 'second';
  }

  // Add information about the number of sidebars.
  if ($vars['layout'] == 'both') {
    $vars['body_classes'] .= ' two-sidebars';
  }
  elseif ($vars['layout'] == 'none') {
    $vars['body_classes'] .= ' no-sidebars';
  }
  else {
    $vars['body_classes'] .= ' one-sidebar sidebar-'. $vars['layout'];
  }
  
	// This  functionality will soon be transferred to OM Tools
  if (!module_exists('om_tools')) {
    $node_type = $vars['node']->type;
    $classes = explode(' ', $vars['body_classes']);
    if (!$vars['is_front']) {
      // Add unique class for each page.
      $path = drupal_get_path_alias($_GET['q']);
      $classes[] = om_id_safe('page-' . $path);
      // Add unique class for each website section.
      list($section) = explode('/', $path, 2);
      if (arg(0) == 'node') {
        if (arg(1) == 'add') {
          $section = 'node-add';                     //section-node-add
          $page_type = arg(2);                       //-page
          $page_type_op = arg(2) . '-add';           //-page-add      
        }
        elseif ((arg(0) == 'node') && is_numeric(arg(1)) && is_null(arg(2))) {
          $page_type = $node_type;                   //-page
          $page_type_op = $node_type . '-view';      //-page-view     
        }
        elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
          $section = 'node-' . arg(2);               //section-node-edit || section-node-delete
          $page_type = $node_type;                   //-page
          $page_type_op = $node_type . '-' . arg(2); //-page-edit or -page-delete       
        }      
        $classes[] = om_id_safe('content-type-' . $page_type);
        $classes[] = om_id_safe('content-type-' . $page_type_op);
      }
      $classes[] = om_id_safe('section-' . $section);
    }
    $vars['body_classes_array'] = $classes;
    $vars['body_classes'] = implode(' ', $classes); // Concatenate with spaces.	
  }
  //dsm($vars);
  //dsm($vars['node']);
}

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function om_preprocess_block(&$vars, $hook) {
  $block = $vars['block'];
  $blocks = block_list($block->region);
  // Additional classes for blocks.
  $block_classes[] = 'block';
  $block_classes[] = 'block-' . $block->module;
  $block_classes[] = 'block-' . $vars['block_zebra'];
  
  $block_classes[] = 'block-' . $vars['block_id'];
  $block_classes[] = 'block-group-' . count($blocks);

  // Block Class Module
  if (module_exists('block_class')) { 
    $block_classes[] = block_class($block); 
  }	  
  	
  if ($vars['block_id'] == 1) {
    $block_classes[] = 'block-first';
  }
	if ($vars['block_id'] == count($blocks)) {
    $block_classes[] = 'block-last';
	}	
	
	// Block Edit
  $vars['edit_block_array'] = array();
  $vars['edit_block'] = '';
  if (user_access('administer blocks')) {
    om_edit_block($vars, $hook);
    $block_classes[] = 'with-edit-block';
  }	
  
  // Block Class Module
  if (module_exists('block_class')) { 
    $block_classes[] = block_class($block); 
  }	
  // Aggregate block classes.
  $vars['block_classes'] = implode(' ', $block_classes);	
  //dsm($vars);
}


function om_edit_block(&$vars, $hook) {
  $block = $vars['block'];
  // Display 'edit block' for custom blocks.
  if ($block->module == 'block') {
    $vars['edit_block_array'][] = l( t('edit block'), 'admin/build/block/configure/' . $block->module . '/' . $block->delta,
      array(
        'attributes' => array(
          'title' => t('Edit this block\'s content.'),
          'class' => 'block-edit',
        ),
        'query' => drupal_get_destination(),
        'html' => TRUE,
      )
    );
  }
  // Display 'configure' for other blocks.
  else {
    $vars['edit_block_array'][] = l( t('configure'), 'admin/build/block/configure/' . $block->module . '/' . $block->delta,
      array(
        'attributes' => array(
          'title' => t('Configure ' . $block->subject),
          'class' => 'block-config',
        ),
        'query' => drupal_get_destination(),
        'html' => TRUE,
      )
    );
  }

  // Display 'edit view' for Views blocks.
  if ($block->module == 'views' && user_access('administer views')) {
    list($view_name, $view_block) = explode('-block', $block->delta);
    $vars['edit_block_array'][] = l( t('edit view'), 'admin/build/views/edit/' . $view_name,
      array(
        'attributes' => array(
          'title' => t('Edit this view.'),
          'class' => 'block-edit-view',
        ),
        'query' => drupal_get_destination(),
        'fragment' => 'views-tab-block' . $view_block,
        'html' => TRUE,
      )
    );
  }
  // Display 'edit menu' for Menu blocks.
  elseif (($block->module == 'menu' || ($block->module == 'user' && $block->delta == 1)) && user_access('administer menu')) {
    $menu_name = ($block->module == 'user') ? 'navigation' : $block->delta;
    $vars['edit_block_array'][] = l( t('edit menu'), 'admin/build/menu-customize/' . $menu_name,
      array(
        'attributes' => array(
          'title' => t('Edit this menu.'),
          'class' => 'block-edit-menu',
        ),
        'query' => drupal_get_destination(),
        'html' => TRUE,
      )
    );
  }
  // Display 'edit menu' for Menu block blocks.
  elseif ($block->module == 'menu_block' && user_access('administer menu')) {
    list($menu_name, ) = split(':', variable_get("menu_block_{$block->delta}_parent", 'navigation:0'));
    $vars['edit_block_array'][] = l( t('edit menu'), 'admin/build/menu-customize/' . $menu_name,
      array(
        'attributes' => array(
          'title' => t('Edit this menu.'),
          'class' => 'block-edit-menu',
        ),
        'query' => drupal_get_destination(),
        'html' => TRUE,
      )
    );
  }

  $vars['edit_block'] = '<div class="edit-block">' . implode(' ', $vars['edit_block_array']) . '</div>';
}

 
/**
 * Generates IE CSS links for LTR and RTL languages.
 */

function om_get_ie_styles($ie) {
  global $language;
  global $theme; 
  switch ($ie) {
    case 'ie' : $ie_path = '/css/ie.css'; break;
    case 'ie6': $ie_path = '/css/ie6.css'; break;
    case 'ie7': $ie_path = '/css/ie7.css'; break;
    case 'ie8': $ie_path = '/css/ie8.css'; break;
    case 'ie9': $ie_path = '/css/ie9.css'; break;
    default   : $ie_path = '/css/ie8.css'; break;
  }
  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . drupal_get_path('theme', $theme) . $ie_path . '" />';

  if ($language->direction == LANGUAGE_RTL) {
    $iecss .= '<style type="text/css" media="all">@import "'. base_path() . drupal_get_path('theme', $theme) .'/css/ie-rtl.css";</style>';
  }
  return $iecss;
}
