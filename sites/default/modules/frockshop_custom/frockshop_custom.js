$(function() {
  /**
   * Full Calendar
   */
  var current_selection = 'from';
  var month = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');

  $('#fullcalendar').parent().show();
  $('#fullcalendar').fullCalendar({
    dayClick: function(date) {
      var timestamp = Date.parse(date) / 1000;
      var d = new Date(date);
      if (current_selection == 'from') {
        $('input[name=from]').val(timestamp);
        $('.from-date').removeClass('from-date');
        $('.day-number', this).addClass('from-date');

        // Need to clear "to" values
        $('.to-date').removeClass('to-date');
        $('input[name=to]').val('');

        // Clear date range and set from date
        $('.date-range').empty().append(month[d.getMonth()] + ' ' + d.getDate() + ' - ');

        current_selection = 'to';
      }
      else if (current_selection == 'to' && timestamp > $('input[name=from]').val()) {
        $('input[name=to]').val(timestamp);
        $('.to-date').removeClass('to-date');
        $('.day-number', this).addClass('to-date');

        // Append to date
        $('.date-range').append(month[d.getMonth()] + ' ' + d.getDate());

        current_selection = 'from';
      }
    },
  }).parent().hide();

  // Set To date if empty when submitted
  $('#date-range').submit(function() {
    if ($('input[name=to]').val() == '') {
      $('input[name=to]').val($('input[name=from]').val());
    }

    // Workaround to set the dates
    $('#date-range input[type=hidden]').each(function(index) {
      var nd = new Date($(this).val() * 1000);
      $(this).val((nd.getMonth() + 1) + '/' + nd.getDate() + '/' + nd.getFullYear());
    });
  });
});
