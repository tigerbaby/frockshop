#product-details-not-for-sale,
#sportco-custom-product-form,
#product-details-anonymous {
  overflow: hidden;
  width: 100%;
}

#sportco-custom-product-form hr {
  height: 1px;
  border: 0;
  border-top: 1px solid #666;
}

#sportco-custom-product-form .table-container {
  overflow: hidden;
  margin-left: 38px;
}

#product-details-not-for-sale table,
#sportco-custom-product-form table,
#product-details-anonymous table {
  table-layout: fixed;
  clear: both;
}

#product-details-not-for-sale table tr,
#sportco-custom-product-form table tr,
#product-details-anonymous table tr {
  background-color: transparent;
  border-bottom: none;
}

#sportco-custom-product-form table tr {
  padding: 0;
}

#product-details-not-for-sale table tr:first-child,
#sportco-custom-product-form table tr:first-child,
#product-details-anonymous table tr:first-child {
  text-transform: uppercase;
}

#product-details-not-for-sale table td,
#sportco-custom-product-form table td,
#product-details-anonymous table td {
  background-color: transparent;
  width: 36px;
  vertical-align: top;
  padding-bottom: 6px;
  text-align: center;
}

#sportco-custom-product-form table tr.second-lace-row td {
  text-align: right;
}

#sportco-custom-product-form table tr.second-lace-row td label,
#sportco-custom-product-form table tr.second-lace-row td select {
  display: inline;
}

#sportco-custom-product-form table tr.second-lace-row td label {
  position: relative;
  top: 8px;
}

#sportco-custom-product-form table td.lace-color {
  text-align: right;
  width: 102px;
}

#sportco-custom-product-form .form-text {
  width: 18px;
  border: 1px solid #000;
  text-align: center;
  height: 15px;
}

#sportco-custom-product-form .form-select {
  padding: 3px 0 2px;
  border: 1px solid black;
  width: 92px;
  margin: auto;
}

#sportco-custom-product-form .form-select.second-lace {
  margin-top: 6px;
  margin-bottom: 6px;
}

/* Colors */
#product-details-not-for-sale .color,
#sportco-custom-product-form .color,
#product-details-anonymous .color {
  margin: auto;
  display: block;
  border: 1px solid #000;
  height: 23px;
  width: 26px;
}

#product-details-not-for-sale .color-container,
#product-details-anonymous .color-container {
  margin-top: 20px;
  overflow: hidden;
  margin-left: 12px;
  float: left;
}

#product-details-anonymous .color-container img {
  margin: 0 3px;
}

#product-details-not-for-sale .color-container {
  display: none;
}

#product-details-not-for-sale .color,
#product-details-anonymous .color {
  float: left;
  border: 1px solid #000;
  margin-right: 12px;
  height: 23px;
  width: 26px;
}

/* Buttons */
.button-container {
  margin-left: 42px;
}

.button-container .left {
  float: left;
  width: 274px;
}

.button-container .left .extras {
  margin-right: 16px;
  color: #903;
  font-size: 14px;
  font-weight: bold;
}

.button-container .right {
  float: left;
}

/* Price representation */
#product-details-not-for-sale .price-representation,
#sportco-custom-product-form .price-representation,
#product-details-anonymous .price-representation {
  padding: 0 12px;
}

#product-details-not-for-sale .price-representation table,
#sportco-custom-product-form .price-representation table,
#product-details-anonymous .price-representation table {
  width: 100%;
}

#product-details-not-for-sale .price-representation table tr,
#sportco-custom-product-form .price-representation table tr,
#product-details-anonymous .price-representation table tr {
  font-size: 16px;
  font-weight: bold;
}

/* Affect first row only */
#product-details-not-for-sale .price-representation table tr.odd,
#sportco-custom-product-form .price-representation table tr.odd,
#product-details-anonymous .price-representation table tr.odd {
  color: #903;
}

#product-details-not-for-sale .link,
#product-details-anonymous .link {
  float: right;
  font-size: 13px;
  font-weight: bold;
  width: 160px;
  margin-right: 16px;
  margin-top: 24px;
}

#product-details-not-for-sale .link {
  text-align: right;
  width: 240px;
  font-size: 13px;
}

/* Filter block */
.filter-sizes {
  text-transform: uppercase;
  overflow: hidden;
  padding: 0 12px 12px;
  color: #333;
}

.clear-filter {
  border-bottom: 1px solid #999;
  padding: 0 12px 12px;
}

.clear-filter a {
  padding: 0;
  border: none;
  text-decoration: underline;
  color: #333;
}

.filter-title {
  color: #903;
  padding: 0 12px;
  font-size: 14px;
  margin-bottom: 14px;
  padding-top: 18px;
}

.filter-sizes .filter-size {
  float: left;
  width: 50%;
}

.clear-all {
  padding: 14px 12px 6px;
}

.clear-all input {
  border: none;
  background: transparent url('images/clear-all.png') 0 0 no-repeat;
  width: 100px;
  height: 26px;
  display: block;
  text-indent: -9999px;
}

.filter-arrow {
  float: right;
  margin-top: 4px;
}

.filter-arrow a {
  display: block;
  width: 10px;
  height: 9px;
  background: transparent url('images/arrow.png') 0 0 no-repeat;
}

.filter-arrow a.collapsed {
  background-image: url('images/arrow-down.png');
}

.filter-colors {
  overflow: hidden;
  padding: 0 12px 12px;
}

.filter-colors .filter-color {
  float: left;
  width: 28px;
  height: 25px;
  padding: 0;
  margin: 0 0 6px;
  display: block;
  font-size: 0;
  line-height: 0;
  margin-right: 3px;
}

.filter-colors .filter-color input {
  display: none;
}

.filter-colors .filter-color img {
  border: 1px solid #666;
  cursor: pointer;
}

.filter-colors .filter-color img.selected {
  border: 1px solid #000;
}

.filter-title.price {
  /*color: #000;*/
}

.filter-prices {
  overflow: hidden;
  padding: 0 12px 12px;
}

.filter-prices label {
  display: block;
  float: left;
}

.filter-prices label.min {
  margin-right: 10px;
}

.filter-material {
  padding: 5px 5px;
}
