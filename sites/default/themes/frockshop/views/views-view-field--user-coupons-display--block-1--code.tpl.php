<?php
// $Id: views-view-field.tpl.php,v 1.1 2008/05/16 22:22:32 merlinofchaos Exp $
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>

<?php
$query = db_query("SELECT * FROM {uc_coupons_orders} AS co LEFT JOIN {uc_orders} AS o ON (co.oid = o.order_id)");


global $user; 
$check = true;
$coup = uc_coupon_find($row->uc_coupons_code);
$us = array();

//dpm(uc_coupon_purchase_has_coupon($coup->data['users'], $row->uc_coupons_code));

if($coup->data['users'] && $row->uc_coupons_status){
	foreach ($coup->data['users'] as $usr){
 		if($usr == $user->uid){
			while($used = db_fetch_object($query)){
				if($used->code == $row->uc_coupons_code && $used->order_status != "in_checkout" ){
					$check = false;
				}
			}
			
			if($check){
				print $row->uc_coupons_value.','.$row->uc_coupons_code;
			}
		}
	}
}
?>
