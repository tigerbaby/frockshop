<?php
// $Id: views-view-unformatted.tpl.php,v 1.6 2008/10/01 20:52:11 merlinofchaos Exp $
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<script>

jQuery(document).ready(function(){
	jQuery('.profile h3:nth-child(8)').hide();
	jQuery('.profile h3:nth-child(8)').next().hide();
});
</script>

<div class="profile">
	<h3>Credits</h3>
	<?php 
		$total = 0;
		$total_coups = array();
		foreach ($rows as $id => $row): ?>
     
        <?php 
		if($row){
			$value =  trim(strip_tags($row, '<div class="views-field-code"><span class="field-content"></span></div><div class="views-field-code-1"><span class="field-content">'));
			
			$trimme = explode(' ', $value);
		}

		$total_coups[] = array('value' => $trimme[0], 'coupon' => end($trimme));
		$total += end($trimme); 
		?>
    <?php endforeach; ?>

    <?php if ($total > 0): ?>
    	<?php foreach ($total_coups as $cp): ?>
        	<?php if($cp['value'] > 0): ?>
    			<strong style='font-size: 14px;'>You have $<?php print $cp['coupon']; ?> credit available with coupon <?php print $cp['value']; ?></strong>
            <br />
			<?php endif; ?>
        <?php endforeach; ?>
    <?php else: ?>
    	<strong style='font-size: 14px;'>You have no credits available.</strong>
    <?php endif; ?>
</div>