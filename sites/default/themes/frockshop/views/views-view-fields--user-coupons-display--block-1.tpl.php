<?php
// $Id: views-view-unformatted.tpl.php,v 1.6 2008/10/01 20:52:11 merlinofchaos Exp $
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php global $user;  
$index = 0;
?>

<?php if ($user->uid): ?>
<?php $user_id = $user-uid; ?>
	<?php foreach ($fields as $id => $field): ?>
   
     	<?php print $field->raw; 
		$index++;
		
		if($index == 2){
			$index = 0;	
		}
		?>
	<?php endforeach; ?>
<?php endif; ?>