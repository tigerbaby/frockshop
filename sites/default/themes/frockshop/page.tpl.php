<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>

	<?php print $head; ?>

	<title><?php print $head_title; ?></title>

	<?php print $styles; ?>

	<!--[if IE  ]><?php print om_get_ie_styles('ie'); ?><![endif]-->

	<!--[if IE 6]><?php print om_get_ie_styles('ie6'); ?><![endif]-->

	<!--[if IE 7]><?php print om_get_ie_styles('ie7'); ?><![endif]-->

	<!--[if IE 8]><?php print om_get_ie_styles('ie8'); ?><![endif]-->

	<!--[if IE 9]><?php print om_get_ie_styles('ie9'); ?><![endif]-->

	<?php print $scripts; ?>

	<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>

</head>

<body class="<?php print $body_classes; ?>">

<?php // automatically generated by OM Subthemer module

$sidebar_first_classes = 'region-sidebar-first row rows-2 row-2-1 row-1 row-first';

$header_top_classes = 'region-header-top row rows-3 row-3-1 row-1 row-first';

$content_top_classes = 'region-content-top row rows-4 row-4-1 row-1 row-first';

$content_classes = 'region-content row rows-5 row-5-1 row-1 row-first';

$content_bottom_classes = 'region-content-bottom row rows-4 row-4-3 row-3';

$footer_classes = 'region-footer row rows-1 row-1-1 row-1 row-first row-last';

?>



<?php /* if (!$logged_in && $is_front) { ?>

<div id="splash-page"><div id="splash-page-inner">

  <?php print theme('region', 'sidebar-first', $sidebar_first, $sidebar_first_classes, 0); ?>

  <?php print $messages; ?>

</div></div><!-- /#splash-page-inner, /#splash-page -->

<?php } else {*/ ?>

<div id="page" class="wrapper wrapper-level-1 wrapper-page row rows-1 row-1-1 row-1 row-first row-last wrapper-outer">

  <div id="page-inner" class="wrapper wrapper-level-2 wrapper-page-inner row rows-2 row-2-1 row-1 row-first">

		<div id="column-left" class="wrapper wrapper-level-3 wrapper-column-left column columns-2 column-2-1 column-1 column-first">

			<?php print theme('identity', $logo, $site_name, $site_slogan, $front_page); ?>

			<?php print theme('region', 'sidebar-first', $sidebar_first, $sidebar_first_classes, 0); ?>

		</div><!-- /#column-left -->

		<div id="main" class="wrapper wrapper-level-3 wrapper-main column columns-2 column-2-2 column-2 column-last">

			<?php print theme('region', 'header-top', $header_top, $header_top_classes, 0); ?>

			<div id="navigation" class="wrapper wrapper-level-4 wrapper-navigation row rows-3 row-3-2 row-2">

				<?php print theme('menu', 'main-menu', $main_menu, $main_menu_tree); ?>

			</div><!-- /#navigation -->

			<div id="main-inner" class="wrapper wrapper-level-4 wrapper-main-inner row rows-3 row-3-3 row-3 row-last">

        <div id="container-top"></div>

				<div id="container" class="wrapper wrapper-level-5 wrapper-container row rows-4 row-4-2 row-2">

          <?php print theme('search_box', $search_box); ?>

				  <?php print theme('region', 'content-top', $content_top, $content_top_classes, 0); ?>

					<?php print theme('content_elements', $mission, $tabs, $title) ?>

					<?php print $help; ?>

					<?php print $messages; ?>

					<?php $price = $node->attributes[15]->options[17]->price;?>

					<?php $english_format_number = number_format($price, 2, '.', '');?>

					<?php $mes = "   Rental Price:  $"?>

					<?php $mesprice = $mes.$english_format_number;?>

					<?php $content = str_replace("Price:","Retail Price:", $content);?>

					<?php $content = str_replace("List Retail Price:",$mesprice, $content);?>

					<?php $content = str_replace("<span class='uc-price-product uc-price-list uc-price'>

<span class='price-prefixes'>Rental Price: $50.00 </span>

$0.00

</span>","<span class='uc-price-product uc-price-list uc-price'>

<span class='price-prefixes'>Rental Price: $50.00 </span></span>", $content);?>

					<?php if (!$is_front) print theme('region', 'content', $content, $content_classes, 0); ?>

          <?php print theme('region', 'content-bottom', $content_bottom, $content_bottom_classes, 0); ?>

          <?php //print $feed_icons; ?>

				</div><!-- /#container -->

        <div id="container-bottom"></div>

			</div><!-- /#main-inner -->

		</div><!-- /#main -->

		<div class="clearfix"></div>

	</div><!-- /#page-inner -->

	<div id="footer-wrapper" class="wrapper wrapper-level-2 wrapper-footer-wrapper row rows-2 row-2-2 row-2 row-last"><div id="footer-wrapper-inner" class="wrapper-inner">

		<?php print theme('region', 'footer', $footer, $footer_classes, 0); ?>

	</div></div><!-- /#footer-wrapper-inner, /#footer-wrapper -->

</div><!-- /#page -->

<?php // } ?>

<?php print $closure; ?>



</body>

</html>

