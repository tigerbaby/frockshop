<?php



function frockshop_status_messages($display = NULL) {

  $output = '';

  foreach (drupal_get_messages($display) as $type => $messages) {

    $output .= "<div class=\"messages $type\">\n";

    if (count($messages) > 1) {

      $output .= " <ul>\n";

      foreach ($messages as $message) {

        if (trim($message) != '') {

          $output .= '  <li>' . $message . "</li>\n";

        }

      }

      $output .= " </ul>\n";

    }

    else {

      $output .= $messages[0];

    }

    $output .= "</div>\n";

  }

  return $output;

}



function frockshop_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {

  // Return original theme function 

  if (arg(0) != 'dresses') return theme_pager($tags, $limit, $element, $parameters, $quantity);

  global $pager_page_array, $pager_total;



  // Calculate various markers within this pager piece:

  // Middle is used to "center" pages around the current page.

  $pager_middle = ceil($quantity / 2);

  // current is the page we are currently paged to

  $pager_current = $pager_page_array[$element] + 1;

  // first is the first page listed by this pager piece (re quantity)

  $pager_first = $pager_current - $pager_middle + 1;

  // last is the last page listed by this pager piece (re quantity)

  $pager_last = $pager_current + $quantity - $pager_middle;

  // max is the maximum page number

  $pager_max = $pager_total[$element];

  // End of marker calculations.



  // Prepare for generation loop.

  $i = $pager_first;

  if ($pager_last > $pager_max) {

    // Adjust "center" if at end of query.

    $i = $i + ($pager_max - $pager_last);

    $pager_last = $pager_max;

  }

  if ($i <= 0) {

    // Adjust "center" if at start of query.

    $pager_last = $pager_last + (1 - $i);

    $i = 1;

  }

  // End of generation loop preparation.



  $li_first = theme('pager_first', (isset($tags[0]) ? $tags[0] : t('« first')), $limit, $element, $parameters);

  $li_previous = theme('pager_previous', (isset($tags[1]) ? $tags[1] : t('‹ previous')), $limit, $element, 1, $parameters);

  $li_next = theme('pager_next', (isset($tags[3]) ? $tags[3] : t('next ›')), $limit, $element, 1, $parameters);

  $li_last = theme('pager_last', (isset($tags[4]) ? $tags[4] : t('last »')), $limit, $element, $parameters);



  if ($pager_total[$element] > 1) {

    // When there is more than one page, create the pager list.

    if ($i != $pager_max) {

      if ($i > 1) {

        $items[] = array(

          'class' => 'pager-ellipsis', 

          'data' => '&hellip;',

        );

      }



      // Now generate the actual pager piece.

      for (; $i <= $pager_last && $i <= $pager_max; $i++) {

        if ($i < $pager_current) {

          $items[] = array(

            'class' => 'pager-item', 

            'data' => theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters),

          );

        }

        if ($i == $pager_current) {

          $items[] = array(

            'class' => 'pager-current', 

            'data' => $i,

          );

        }

        if ($i > $pager_current) {

          $items[] = array(

            'class' => 'pager-item', 

            'data' => theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters),

          );

        }

      }



      if ($i < $pager_max) {

        $items[] = array(

          'class' => 'pager-ellipsis', 

          'data' => '&hellip;',

        );

      }

    }

    // End generation.

    $items[] = array(

      'data' => '',

      'style' => 'display: block;',

    );



    if ($li_first) {

      $items[] = array(

        'class' => 'pager-first', 

        'data' => $li_first,

      );

    }

    if ($li_previous) {

      $items[] = array(

        'class' => 'pager-previous', 

        'data' => $li_previous,

      );

    }

    if (array_pop(arg()) != 'all') {

      $get_size = $_GET['size'];

      $get_color = $_GET['color'];

      if (!empty($get_size) || !empty($get_color)) {

        $items[] = array(

          'class' => 'pager-all',

          'data' => '<a href="' . $_GET['q'] . '/all' . ((!empty($get_size)) ? '?size=' . $get_size : '') . ((!empty($get_color)) ? '?color=' . $get_color : '') . '" class="active">View all</a>',

        ); 

      } else {

        $items[] = array(

          'class' => 'pager-all',

          'data' => l(t('View All'), $_GET['q'] . '/all' . ((!empty($get_size)) ? '?size=' . $get_size : '') . ((!empty($get_color)) ? '?color=' . $get_color : ''), array('attributes' => array('class' => 'active'))),

        );

      }

    }

    if ($li_next) {

      $items[] = array(

        'class' => 'pager-next', 

        'data' => $li_next,

      );

    }

    if ($li_last) {

      $items[] = array(

        'class' => 'pager-last', 

        'data' => $li_last,

      );

    }



    return theme('item_list', $items, NULL, 'ul', array('class' => 'pager'));

  }

}

function frockshop_preprocess_page(&$variables) {
  if ($variables['node']->type == "homepage") {
    $variables['template_files'][] = 'page-node-homepage';
  }
}
