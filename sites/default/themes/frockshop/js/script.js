//OM Subtheme script

$(document).ready( function(){

  $('#user-login-form #edit-name').val('e-mail');
  $('#user-login-form #edit-pass').val('password');
  
  $('#user-login-form #edit-name').click(function() {
  $('#user-login-form #edit-name').val('');
  });
  $('#user-login-form #edit-pass').click(function() {
  $('#user-login-form #edit-pass').val('');
  });
  
  $('#edit-attributes-1').attr("disabled",true);  
  $('#edit-attributes-2').attr("disabled",true);
  
  
  /**
   * Set width
   */
  $('#splash-image img').width((window.document.documentElement.clientWidth || window.document.body.clientWidth) || window.innerWidth);

  $(window).resize(function() {
    $('#splash-image img').width((window.document.documentElement.clientWidth || window.document.body.clientWidth) || window.innerWidth);
  });
})


Drupal.behaviors.togg = function (context) {
  $('div.togg:not(.toggdiv-processed)', context).each(function () {
    $(this).addClass('toggdiv-processed').hide();
  });
  $('.togg-link:not(.togg-processed)', context).addClass('togg-processed').each(function () {
    $(this).click(function() {
      $(this).next('.togg').toggle(400);
      return false;
    });
  });
};



Drupal.behaviors.mrc_browser = function () {
  // Trigger lightbox for add to cart button
  $(".add-to-cart .ajax-cart-submit-form .ajax-cart-submit-form-button").bind("click", function() {
    $('h1.title').ajaxComplete(function(e, xhr, settings) {
      if (settings.url == '/uc_ajax_cart/add/item') {
        if (/messages error/.test(xhr.responseText)) {
          // Remove messages
          $('.messages').remove();

          // Set message
          $(this).after(xhr.responseText);
        }
        else {
          Lightbox.triggerLightbox('lightframe', 'addtocart');
          $("#lightbox2-overlay").addClass('hide-overlay');
          $("#lightbox.lightbox2-orig-layout").addClass('uniq-lightbox');

          $('#frameContainer iframe').load(function() {
            $(this).attr('style', 'display: inline !important; border: none; z-index: 10500;');
          });
        };
      }
    });
  });
}


