<?php global $user;

//redirect logged in users
if ($user->uid) {
	drupal_goto('welcome-to-frock-shop-chicago');
}


//for showing to admin if ($user->uid && $user->uid != 9) {

/* for showing admin the page
if ($user->uid && $user->uid == 9) {
	print 'redirect logged in user. you are not redirected becuase you are logged in as admin';
} */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>

	<?php print $head; ?>

	<title><?php print $head_title; ?></title>

	<?php print $styles; ?>

	<!--[if IE  ]><?php print om_get_ie_styles('ie'); ?><![endif]-->

	<!--[if IE 6]><?php print om_get_ie_styles('ie6'); ?><![endif]-->

	<!--[if IE 7]><?php print om_get_ie_styles('ie7'); ?><![endif]-->

	<!--[if IE 8]><?php print om_get_ie_styles('ie8'); ?><![endif]-->

	<!--[if IE 9]><?php print om_get_ie_styles('ie9'); ?><![endif]-->

	<?php print $scripts; ?>

	<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>

</head>

<body class="<?php print $body_classes; ?>">



<?php // automatically generated by OM Subthemer module

$sidebar_first_classes = 'region-sidebar-first row rows-2 row-2-1 row-1 row-first';

$header_top_classes = 'region-header-top row rows-3 row-3-1 row-1 row-first';

$content_top_classes = 'region-content-top row rows-4 row-4-1 row-1 row-first';

$content_classes = 'region-content row rows-5 row-5-1 row-1 row-first';

$content_bottom_classes = 'region-content-bottom row rows-4 row-4-3 row-3';

$footer_classes = 'region-footer row rows-1 row-1-1 row-1 row-first row-last';

?>





<div id="splash-image">

<img src="/<?php print $node->field_background_image[0]['filepath']; ?>" alt="Frock Shop Chicago" />

</div>

<div id="splash-page"><div id="splash-page-wrapper">

  <?php  ?>
<h1 id="home-title"><?php print $node->title; ?></h1>
 
  
  <div id="overlay-text">
  	<?php print $node->field_overlay_text[0]['value']; ?>
  </div>
  
  
  <div id="homepage-panel">
  	
  	<?php print $node->field_display_text[0]['value']; ?>
    <a href="/welcome-to-frock-shop-chicago" id="enter">Enter</a>
	
    <img id="home-cursive" src="/sites/default/themes/frockshop/css/images/cursive-home-text.png" alt="rental dresses for spcial occasion" />
    
    <div id="product-slider">
         <?php print theme('region', 'content-bottom', $content_bottom, $content_bottom_classes, 0); ?> 
			<?php //$viewName = 'homepage_product_slider';
                   //  print views_embed_view($viewName); ?>
        
	</div>
	

    
    
  </div>
   <?php print $messages; ?>
  
  <?php //print theme('region', 'sidebar-first', $sidebar_first, $sidebar_first_classes, 0); ?>



</div></div><!-- /#splash-page-inner, /#splash-page -->

<?php /*
 
<pre>
<?php print_r($node); ?>
</pre>


*/?>



<?php print $closure; ?>



</body>

</html>
